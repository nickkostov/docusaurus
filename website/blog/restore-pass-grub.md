---
slug: pass-via-grub
title: Restore password via grub
author: Nikolay Kostov
author_title: Maintainer of Docusaurus
author_url: https://github.com/nickkostov
author_image_url: https://avatars.githubusercontent.com/u/35745114?s=60&v=4
tags: [pass-via-grub, docusaurus]
---