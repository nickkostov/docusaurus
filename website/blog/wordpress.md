---
slug: wordpress
title: Wordpress-RedHat-8
author: Nikolay Kostov
author_title: Maintainer of Docusaurus
author_url: https://github.com/nickkostov
author_image_url: https://avatars.githubusercontent.com/u/35745114?s=60&v=4
tags: [wordpress, docusaurus]
---

Софтуерни изисквания:
:::danger 
RHEL 8 / CentOS 8
MariaDB Server 10.3.10, PHP 7.2.11-1, Apache/2.4.35 (Red Hat Enterprise Linux)
Привилигирован достъп къв вашата линукс среда.
:::

Защо бихме искали да инсталираме Wordpress и какво е?


```bash
dnf install php-mysqlnd php-fpm mariadb-server httpd tar curl php-json
```

```bash
firewall-cmd --permanent --zone=public --add-service=http 
```

```bash
firewall-cmd --permanent --zone=public --add-service=https
```

```bash
firewall-cmd --reload
```

```bash
systemctl start mariadb
```

```bash
systemctl start httpd
```

```bash
systemctl enable mariadb
```

```bash
systemctl enable httpd
```

```bash
mysql_secure_installation
```

```bash
mysql -u root -p
```

```sql
CREATE DATABASE wordpress;
```

```sql
CREATE USER `admin`@`localhost` IDENTIFIED BY 'pass';
```

```sql
GRANT ALL ON wordpress.* TO `admin`@`localhost`;
```

```sql
FLUSH PRIVILEGES;
```

```sql
exit
```

```bash
curl https://wordpress.org/latest.tar.gz --output wordpress.tar.gz
```

```bash
tar xf wordpress.tar.gz
```

```bash
tar xf wordpress.tar.gz
```

```bash
cp -r wordpress /var/www/html
```

```bash
chown -R apache:apache /var/www/html/wordpress
```

```bash
chcon -t httpd_sys_rw_content_t /var/www/html/wordpress -R
```
