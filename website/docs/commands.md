---
title: Команди.
slug: commands 
author: Nikolay Kostov
---
#####  Команди

Смяна на потребителя

```bash
su - {username}
``` 
Създава папка:

```bash
mkdir {directory name}
```  
Добавяне на група:

```bash
groupadd {groupname}
```

Добавете потребителя към допълнителните
групи. Винаги с -аG ако добавяте група към

```bash
usermod -aG {group} {username} 
```

===========================================================================
##### Екстра команди

Създава един файл по ваше наименование или може да направи много файлове ако
добавите преди extension на файла {1..което и
да е чило = ще отговаря на максималния брой
файлове}

```bash
touch {filename or filename{1..-} } 
```

Добавяте потребител в някой системи
интерактивно в някой не, използвайте
внимателно: 

```bash
useradd {username }
```

Промяна на паролата за даден потребител:

```bash
passwd {username}
``` 

Добавя нов потребител интерактивно (Ubuntu-tested)

```bash
adduser {username}
```

Променяте правата на папка и файловото
съдържание:

```bash
chmod -R {permissions} {folder}/
``` 

```bash
usermod -G {group} {username} 
```




===========================================================================


===========================================================================

##### Задачка

Задачка посочете как ще опростите до една команда:

```bash
mkdir -p Projects/ancient
mkdir Projects/classical
mkdir Projects/medieval
```

```bash
mkdir Projects/ancient/egyptian
mkdir Projects/ancient/nubian
mkdir Projects/classical/greek
mkdir Projects/medieval/britain
mkdir Projects/medieval/japan
```

```bash
touch Projects/ancient/nubian/further_research.txt
touch Projects/classical/greek/further_research.txt
```

```bash
mv Projects/classical Projects/greco-roman
```

===========================================================================



