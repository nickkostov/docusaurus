---
title: Филтри в Линукс
slug: filters/filters
author: Nikolay Kostov
---

```tr``` командата:

Конвертира, извлича и изтрива символи и знаци.
Примера показва трансформация от малки символи към големи.


```bash 
echo "Welcome to this test file. Hello there.What is up?" > file-example.txt

cat file-test.txt | tr "[a-z]" "[A-Z]"

```

Същия пример – друг метод на изписване:

```bash

cat file-test.txt | tr "[:lower:]" "[:upper:]"


[nick@workstationrh ~]$ cat file-test.txt | tr "[:lower:]" "[:upper:]"
HELLO THERE!

HOW ARE YOU

```

Пример в който превръщам space в табулация:

```bash

echo "Welcome to this test file" | tr [:space:] '\t' > file-test.txt

Welcome to      this    test    file    [nick@workstationrh ~]$

```

Трансформация и записването и в нов файл.

```bash
[nick@workstationrh ~]$ echo "WELCOME TO THE TESTFILE" | tr [:space:] '\t' > fileexample
[nick@workstationrh ~]$ cat fileexample
WELCOME TO      THE     TESTFILE        [nick@workstationrh ~]$
```

===========================================================================

##### Сортиране със `sort`
``sort`` командата:

Направете един фаил и впишете в него даденият лист:

```bash
Apples
Pears
Bananas
Oranges
Laptop
```

```bash
touch producs.txt
```
 

Сортира по азбучен ред думи:

```bash
[nick@workstationrh ~]$ sort products
Apples
Bananas
Laptop
Oranges
Pears
```


Пример с прехвърляне на изхода:

```bash
[nick@workstationrh ~]$ sort products > products-sorted
[nick@workstationrh ~]$ cat products-sorted
Apples
Bananas
Laptop
Oranges
Pears

[nick@workstationrh ~]$ cat products
Apples
Pears
Bananas
Oranges
Laptop
[nick@workstationrh ~]$
```

Сортиране в обърнат ред:

```bash
[nick@workstationrh ~]$ cat products-sorted
Apples
Bananas
Laptop
Oranges
Pears

[nick@workstationrh ~]$ sort -r products-sorted
Pears
Oranges
Laptop
Bananas
Apples
```


Случаен принцип:

```bash
[nick@workstationrh ~]$ sort -r products-sorted
Pears
Oranges
Laptop
Bananas
Apples

[nick@workstationrh ~]$ sort -R products-sorted
Apples
Pears
Bananas
Laptop
Oranges

[nick@workstationrh ~]$ sort -R products-sorted
Pears
Apples
Laptop
Oranges
Bananas

[nick@workstationrh ~]$ sort -R products-sorted
Oranges
Pears
Bananas
Laptop
Apples
```
Сортиране на определени от нас колони:

```bash
[nick@workstationrh ~]$ cat movies-random
Fish Movies
Food Iron-Man
Bread Film
Chrisps Cinematic
Potato Thor

[nick@workstationrh ~]$ sort -k 1 movies-random
Bread Film
Chrisps Cinematic
Fish Movies
Food Iron-Man
Potato Thor

[nick@workstationrh ~]$ sort -k 2 movies-random
Chrisps Cinematic
Bread Film
Food Iron-Man
Fish Movies
Potato Thor
```

##### Забавления със `cut`

cut:

Извлича избрани полета от редове текст (използва табулация за разделител по
подразбиране). Работи най-добре със структуриран текст (текст с колони):

В примера се случва следното нещо буквално: режа файла на раздели които се делят от ``:`` и изисквам определени полета към този пример могат да се ползват ``/`` и например ``x`` : 

```bash
[nick@workstationrh ~]$ cut -d: -f 1,3 /etc/passwd
root:0
bin:1
daemon:2
adm:3
lp:4
sync:5
shutdown:6
halt:7
mail:8
operator:11

```

Ако исках да отпечатам първото поле на всяко име на файл, щях да използвам тази команда:


```bash
[nick@workstationrh ~]$ ls -1 | cut -f1 -d'.'
Course
Desktop
Documents
Downloads
Music
Pictures
Projects
Public
Repo
Templates
Videos
error
file
file-test
fileexample
files
movies-random
products
products-sorted
script
snap
test
```

Ако исках да отпечатам второто поле на всяко име на файл, щях да използвам тази команда:


```bash
[nick@workstationrh ~]$ ls -1 | cut -f2 -d'.'
Course
Desktop
Documents
Downloads
Music
Pictures
Projects
Public
Repo
Templates
Videos
log
file
txt
fileexample
files
movies-random
products
products-sorted
sh
snap
sh
```

```bash
[nick@workstationrh ~]$ cut -f1 -d: /etc/passwd
root
bin
daemon
adm
lp
sync
shutdown
halt
mail
operator
games
ftp
nobody
dbus
systemd-coredump
systemd-resolve
```

Има и опция в която можем да ползваме ``cut`` с разделител ' ' което е буквално един space, само ако искаме да покажем на командата, че разделението е със спейс.

Примерен текст:

Apples are red.
Apples are green.
The dress is yellow.
The idea sounds briliant.
Linux is awesome.

```bash
[nick@workstationrh ~]$ cat delimeter
Apples are red.
Apples are green.
The dress is yellow.
The idea sounds briliant.
Linux is awesome.

[nick@workstationrh ~]$ cut -d' ' -f1 delimeter
Apples
Apples
The
The
Linux

[nick@workstationrh ~]$ cut -d' ' -f2 delimeter
are
are
dress
idea
is

[nick@workstationrh ~]$ cut -d' ' -f3 delimeter
red.
green.
is
sounds
awesome.

[nick@workstationrh ~]$ cut -d' ' -f4 delimeter


yellow.
briliant.

[nick@workstationrh ~]$ cut -d'.' -f4 delimeter





[nick@workstationrh ~]$

```

Можем и по байт което на практика със текстови файл е просто по дума.

```bash
[nick@workstationrh ~]$ cut -b 2 delimeter
p
p
h
h
i
[nick@workstationrh ~]$ cut -b 1 delimeter
A
A
T
T
L
[nick@workstationrh ~]$ cut -b 10 delimeter
e
e

s
a
[nick@workstationrh ~]$ cut -b 12 delimeter
r
g
s
u
e
```

Можем и по знак:

```bash
[nick@workstationrh ~]$ cut -c 1,3 delimeter
Ap
Ap
Te
Te
Ln

[nick@workstationrh ~]$ cut -c 1,2 delimeter
Ap
Ap
Th
Th
Li

[nick@workstationrh ~]$ cut -c 1,2,3 delimeter
App
App
The
The
Lin

[nick@workstationrh ~]$ cut -c 1,2,3,4,5 delimeter
Apple
Apple
The d
The i
Linux

[nick@workstationrh ~]$ cut -c 1,2,3,4,5,7,8,9,10 delimeter
Apple are
Apple are
The dess
The iea s
Linuxis a
```

Можем и със опцията --complement което значи допълнение, аз я разбирам без определени занаци/букви да е:

```bash

[nick@workstationrh ~]$ cut --complement -c 1 delimeter
pples are red.
pples are green.
he dress is yellow.
he idea sounds briliant.
inux is awesome.

[nick@workstationrh ~]$ cut --complement -c 1,2 delimeter
ples are red.
ples are green.
e dress is yellow.
e idea sounds briliant.
nux is awesome.

[nick@workstationrh ~]$ cut --complement -c 2 delimeter
Aples are red.
Aples are green.
Te dress is yellow.
Te idea sounds briliant.
Lnux is awesome.

```