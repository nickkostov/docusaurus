---
title: Регулярни изрази и глобиране
slug: filters/reg-ex
author: Nikolay Kostov
---

## Регулярни изрази

Регулярните изрази позволяват фина спецификация на търсенията и се поддържат от
множество популярни команди и езици за програмиране.

• ```grep g..m /etc/passwd```: 

```bash
[nick@workstationrh ~]$ grep g..m /etc/passwd
gnome-initial-setup:x:976:974::/run/gnome-initial-setup/:/sbin/nologin
```
• Точката обозначава един знак - както виждате в примера отгоре липсват два знака от думичката gnome.


• ^ търси в началото на линията - както виждате изважда попадения със знака `n`:

```bash
[nick@workstationrh ~]$ grep ^n /etc/passwd
nobody:x:65534:65534:Kernel Overflow User:/:/sbin/nologin
nick:x:1000:1000:Nikolay Kostov:/home/nick:/bin/bash
nginx:x:974:972:Nginx web server:/var/lib/nginx:/sbin/nologin
```

• $ търси в края на линията - както е показано в примера по долу:

```bash
[nick@workstationrh ~]$ grep nologin$ /etc/passwd

bin:x:1:1:bin:/bin:/sbin/nologin
daemon:x:2:2:daemon:/sbin:/sbin/nologin
adm:x:3:4:adm:/var/adm:/sbin/nologin
lp:x:4:7:lp:/var/spool/lpd:/sbin/nologin
mail:x:8:12:mail:/var/spool/mail:/sbin/nologin
operator:x:11:0:operator:/root:/sbin/nologin
games:x:12:100:games:/usr/games:/sbin/nologin
ftp:x:14:50:FTP User:/var/ftp:/sbin/nologin
nobody:x:65534:65534:Kernel Overflow User:/:/sbin/nologin
dbus:x:81:81:System message bus:/:/sbin/nologin
systemd-coredump:x:999:997:systemd Core Dumper:/:/sbin/nologin
systemd-resolve:x:193:193:systemd Resolver:/:/sbin/nologin
```

• grep ^[Aa].[Aa] /etc/passwd:

```bash
[nick@workstationrh ~]$ grep ^[Aa].[Aa] /etc/passwd

avahi:x:70:70:Avahi mDNS/DNS-SD Stack:/var/run/avahi-daemon:/sbin/nologin
```

• man 7 regex .

## Wildcards (file globbing) 

:::tip Предлагат ни по лек начин да намираме неща които не знаем как се казват.
:::

• Wildcards по-точно е символ който може да обозначава един или повече знака.

• ? – всеки единичен символ, ако ползвате само един ще се опита само един да извади, ако са повече ще търси дума която има по-голям брой знаци:


```bash
[nick@workstationrh globiing]$ ls ?????.txt

junk1.txt  junk3.txt  junk5.txt  test2.txt  test4.txt  test6.txt  test8.txt
junk2.txt  junk4.txt  test1.txt  test3.txt  test5.txt  test7.txt  test9.txt
```

• * - нула или повече последователни символа:

```bash
[nick@workstationrh globiing]$ ls *.txt
junk1.txt  junk3.txt  junk5.txt  test10.txt  test3.txt  test5.txt  test7.txt  test9.txt
junk2.txt  junk4.txt  test1.txt  test2.txt   test4.txt  test6.txt  test8.txt
```

Ситуации с числа и букви:

``[Aa]`` ```[0-9]```

```bash
[nick@workstationrh globiing]$ ls [Jj]*.txt
junk1.txt  junk2.txt  junk3.txt  junk4.txt  junk5.txt
```

```bash
[nick@workstationrh globiing]$ ls [Ff]ile[Rr]eport??DataFor202[0-9]?20*.odit

FileReportedDataFor2020-2021.odit
```
