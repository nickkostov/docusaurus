---
title: Команди за намиране на неща
slug: localization-commands/localization 
author: Nikolay Kostov
---
##### Команди с цел откриване и локализиране:

• locate : комадна която търси в локаната база данни за папки и файлове които съвпадат
определен критерии.

Ако командата липсва вероятно не инсталиран пакета:

Ubuntu: ```sudo apt-get install mlocate```


RedHat: ```sudo yum -y install mlocate```

- синтаксис:

```bash
locate {file-name-or-path}

```

Ограничете броя на резултатите до 15:

```bash
locate -n 20 "filename.exten" 

```

Използване на wildcards:

```bash

locate -n 20 "filename*.exten"

```


Пренебрегване на случая:


```bash

locate -n 20 -i "fiLEname*.Exten"

```

Дори с регулярен израз (най-напреднал):
Намерете целия файл в моята домашна папка с текста ssh в пътя на файла:

```bash

locate -r "^/home/user/.*ssh.*$"

```


• updatedb : опреснява локаната база с данни която locate ползва.

```bash
[nick@workstationrh ~]$ locate Projects/

/home/nick/Projects/files
/home/nick/Projects/files-2
/home/nick/Projects/files/Projects
/home/nick/Projects/files/config
/home/nick/Projects/files/config1
/home/nick/Projects/files/hard
/home/nick/Projects/files/hardlinkfile
/home/nick/Projects/files/Projects/configuration-file-shortcut
/home/nick/Projects/files/Projects/pseudo-config-folder
/home/nick/Projects/files/Projects/pseudo-config-folder/configuration-file-original
/home/nick/Projects/files/hard/file-example-hard-link
/home/nick/Projects/files/hard/hardlinked-file

[nick@workstationrh ~]$ sudo updatedb

```

• whereis : това е команда която локализира бинарните файлове, източници и наръчници за команда.

```bash
nick@Beast:/mnt/c/Users/Nick/Documents$ whereis bin
bin: /usr/local/bin

nick@Beast:/mnt/c/Users/Nick/Documents$ whereis bash
bash: /usr/bin/bash /etc/bash.bashrc /mnt/c/Windows/system32/bash.exe /usr/share/man/man1/bash.1.gz

nick@Beast:/mnt/c/Users/Nick/Documents$ whereis top
top: /usr/bin/top /usr/share/man/man1/top.1.gz

nick@Beast:/mnt/c/Users/Nick/Documents$ whereis ls
ls: /usr/bin/ls /usr/share/man/man1/ls.1.gz

nick@Beast:/mnt/c/Users/Nick/Documents$ whereis cd
cd:

nick@Beast:/mnt/c/Users/Nick/Documents$ whereis ifconfig
ifconfig: /usr/sbin/ifconfig /usr/share/man/man8/ifconfig.8.gz
```


===========================================================================



##### Командата find:

Намира всичко в директорията и под нея find {directory name} Намирате всички директории свързани с
нея:

```bash
find .
```

Ще намери всички директории без
файлове: 

```bash
find . –type d
``` 

Търси само файлове:

```bash
find . –type f 
```

Търси файлове по дадено име:

```bash
find . –type f –name {filename}
``` 

Търи файла глобирайки го (wildcard):
```bash
sudo find -type f -name “{filrename}*” 
```

Търси файла и премахва case sensitive
случаите:

```bash
sudo find . -type f -iname “{filename*}” 
```

Може и с глобиране:

```bash
find -type f -iname "*.txt" 
```


Търси последно модифицираните файлове примера е с 10 минути:
```bash
find . -type f -mmin -10 
```

Опцията която търси време дни за модификацийте.
```bash
find -mmtime 
```

Търси по зададен размер:

```bash
find -size option find . -size +5M 
```

Търси само празните файлове – без
съдържание:

```bash
find . –empty 
```

Търси по зададени права:

```bash
find –perm 777 или 666
```

Тръси даден файл и му прилага права или
команда. Тук е с промяна на групата:

```bash
find webin -exec chown ivan:sudo {} +
```

Тръси даден файл и му прилага права или
команда. Тук е с промяна на правата:

```bash
find webin -type f -exec chmod 660 {} + 
```


