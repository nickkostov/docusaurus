---
title: MBR уреди за създаване на дялове
slug: mbr-paritioning-tools
author: Nikolay Kostov
---

## Команди с които можем да преглеждаме дисковите у-ва:

### Командата `lsblk`:

- Това е команда с която можем да си прегледаме блоковите у-ва.
    - Блоково у-во може да е всяко парче хардуеър което има данни записани върху него в блоковви големи парчета.
    - Твърдите дискове са тези части от хардуеъра които попадат в тази категория.

```bash
lsblk
```

**Примерен изход на lsblk**

```
NAME        MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
loop0         7:0    0 140.7M  1 loop /var/lib/snapd/snap/gnome-3-26-1604/100
loop1         7:1    0  99.2M  1 loop /var/lib/snapd/snap/core/10859
sr0          11:0    1  1024M  0 rom  
nvme0n1     259:0    0    20G  0 disk 
├─nvme0n1p1 259:1    0   300M  0 part /boot
├─nvme0n1p2 259:2    0     2G  0 part [SWAP]
└─nvme0n1p3 259:3    0  17.7G  0 part /
```

### Командата `fdisk`:

- Завещана команда, която е била ползвана за създаването на дискови дялове от типа MBR или GPT.


- Изпълнявайки командата, ще ни отвори интерактивният начин на създаване на дискове:

```bash
fdisk /dev/{disk-taken-from-(lsblk)}
```

- Веднага след като, влезем в `fdisk` менюто ще забележим, че можем да ползваме `m` за да видим помощника.
    - Опцията посочва всички възможности на `fdisk`:


**Примерен изход на fdisk с опцията m**

```bash
Help:
  DOS (MBR)
   a   toggle a bootable flag
   b   edit nested BSD disklabel
   c   toggle the dos compatibility flag
  Generic
   d   delete a partition
   F   list free unpartitioned space
   l   list known partition types
   n   add a new partition
   p   print the partition table
   t   change a partition type
   v   verify the partition table
   i   print information about a partition
  Misc
   m   print this menu
   u   change display/entry units
   x   extra functionality (experts only)
  Script
   I   load disk layout from sfdisk script file
   O   dump disk layout to sfdisk script file
  Save & Exit
   w   write table to disk and exit
   q   quit without saving changes
  Create a new label
   g   create a new empty GPT partition table
   G   create a new empty SGI (IRIX) partition table
   o   create a new empty DOS partition table
   s   create a new empty Sun partition table
```

- С `p` ще можем да видим как текущата ни дялова таблица изглежда

**Примерен изход**

```bash
Disk /dev/nvme0n1: 20 GiB, 21474836480 bytes, 41943040 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0xf4bbc120

Device         Boot   Start      End  Sectors  Size Id Type
/dev/nvme0n1p1 *       2048   616447   614400  300M 83 Linux
/dev/nvme0n1p2       616448  4810751  4194304    2G 82 Linux swap / Solaris
/dev/nvme0n1p3      4810752 41943039 37132288 17.7G 83 Linux
```

- Създаване на нов дял:

```bash
fdisk /dev/{new-empty-disk-drive}
```

1. Ползваме опцията `n`.
- Ще видим, че ще имаме две опцийки:
```bash
Partition type:
   p   primary (0 primary, 0 extended, 4 free)
   e   extended
Select (default p)
```
2. Избираме `p`, за да създадем primary.
3. Избираме 1.
4. Сега ще ни пита за пъвият сектор, избираме `default`.
5. Сега пита за последният пак избираме: `default`.
6. За да проверим прясно създаденият ни дял: `p`.
7. Това което ще видим:

Device Boot: Дяла.

Start: Началото на цилиндъра.

End: Края

Blocks: Колко блока има в дяла.

ID: Вида на файловата система

System.

- Дялови IDта:
    - 83 Стандартна линукс файлова система.
    - 82 Линукс Swap дял.
    - 8е Линукс LVM обем.

8. За да запишем процеса използваме `w`, с цел да запишем фаловата система към този диск.


```bash
fdsik /dev/path/to/disk -l
```

### Командата `parted`:

- Друг уред с цел създаване на дискови дялове.

```bash 
parted /dev/diskname
```
- Както виждате, също имаме интерактивно меню.

- Опцията `p` показва дяловете.

- Опцията `mklabel` дава етикет на дяла.

- Опцията `mkpart` създава нов дял.

- Отново имаме опцията, за primary/extended.

- По подразбиране е имаме опцията да сложим `ext2`, но можем да изберем друг тип файлова система.

- Ще имаме опцията `Start` можем да впишем 1 с цел да покажем, че искаме да стартираме дяла на първият megabyte.

- След това ще имаме `End` където можем да сложим например 1000 което е равно на 1GB.

- След това пак можем да погледнем с `p` какво сме създали.
