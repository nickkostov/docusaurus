---
title: Zypper
slug: package-managers/zypper
author: Nikolay Kostov
---

- Ползва се в Suse Linux дистрибуцийте.
    - Примери:

```bash
zypper repos
```

```bash
zypper install <package-name>
```