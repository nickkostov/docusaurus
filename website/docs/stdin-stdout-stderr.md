---
title: Стандартен изход, вход и грешка
slug: stdin-out-err
author: Nikolay Kostov
---




## stdin/out/err

:::tip Кофичката
Във всички UNIX базирани системи имат нещо като “кофа” за всичкият изход от
команди. “Кофата” се нарича още standard output.
Абривиацията е 
'stdout' ===> Стандартен изход.
:::

Пример:
Това което правя оттдолу е прескачане на “кофата” и прехвърлянето на информацията

от ls -lah командата във файл с име bucket.

```bash
ls -lah

ls -lah > bucket

cat bucket
```



:::tip Също можем да извършваме във една комада действието с piping:
 
 Piping – позволява STDOUT (изход) на една програма (лявата на pipe) да стане STDIN
(вход) на друга (дясната на pipe). Символът за pipe е `` | ``.

:::


===========================================================================

`tee` – позволява едновременно запис към STDOUT и към файл.

Стандартен вход:

Стандартиният вход (standard input) е най често зададен от нас използвайки
клавиатурата – пишейки командите.

Но може да бъде и от приложение/deamon. Файлове и стандартен вход могат да се
ползват в стандартният вход на други команди:

• със <, |

• Абривиация – `stdin`

• Примерно ``wc < test.sh``

```wc``` брои думите

```bash
touch test.sh

[nick@workstationrh ~]$ wc < test.sh
0 0 0
```

• cat /etc/passwd | less

Стандартна Грешка:

Индфикационнен номер ако нещата не са направени както трявбва.

• Абривиация ‘stderr’

• Попринцип е излиза на екрана като стандартен изход

• Има ‘файлова дръжка’ – цифра с която е свързана – 2

• stdin е 0

• stdout е 1

```bash

[nick@workstationrh ~]$ wc < test.shsadsd
-bash: test.shclear: No such file or directory
[nick@workstationrh ~]$ echo $?
1
[nick@workstationrh ~]$
```

:::caution Как изглежда
```touch script.sh```

```echo "apt updatdasd" > script.sh```

```chmod u+x script.sh```

```./script.sh == виждаме грешката директно на екрана```

```[nick@workstationrh ~]$ ./script.sh```

```./script.sh: line 1: apt: command not found```

```./script.sh 2> error.log пращаме грешката във файл```

```[nick@workstationrh ~]$ ./script.sh```

```./script.sh: line 1: apt: command not found```

```[nick@workstationrh ~]$ ./script.sh 2> error.log```
```[nick@workstationrh ~]$ cat error.log```

```./script.sh: line 1: apt: command not found```

```./script.sh 2>&1 | less``` == отваряме грешката дирекнто на екрана с less

:::

===========================================================================

``Tee`` командата:
Командата ‘tee’ чете данните от ‘stdin’ и записва данните в ‘stdout’. Командата е много
полезна за връзване на много команди поглеждайки ‘stdout’ на различни етапи.

```bash
find . -name 'test*' | tee found.log | sort -n | tee sorted.log
```