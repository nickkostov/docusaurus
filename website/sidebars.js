module.exports = {
  docs: [
    {
      type: 'category',
      label: 'Информация',
      items: ['getting-started',],
    },
    {
      type: 'category',
      label: 'Линукс',
      items: [
      'part-one',
      'part-two',
      'commands',
      'stdin-stdout-stderr',
      'file-structures',
      'umask',
      'init',
      'upstart',
      'systemd',
      'boot-logs',
      'screen-managment',
      'processes',
      'runlevels',
      'design-hard-disk',
      'lvm',
      'virtualization-containers',
      'file-compression',
      'bash-scripting',
      'mbr-paritioning-tools',
      'gpt-partitioning',
      'swapdisks',
      'setting-up-the-shell-env',
      'x-11',
      'networking',
    ],
    },
    {
      type: 'category',
      label: 'Пакетни мениджъри',
      items: ['package-managers/apt','package-managers/rpm','package-managers/yum','package-managers/zypper','package-managers/dnf','package-managers/dpkg'],
    },
    {
      type: 'category',
      label: 'Автоматизация на задачи',
      items: ['automate-system-tasks/at','automate-system-tasks/cronjobs','automate-system-tasks/systemd-timers',],
    },
    {
      type: 'category',
      label: 'Логове',
      items: ['logs/logs'],
    },
    {
      type: 'category',
      label: 'Докер',
      items: ['docker/docker-part-one'],
    },
    {
      type: 'category',
      label: 'Линукс Филтри',
      items: ['filters/filters','filters/reg-ex'],
    },
    {
      type: 'category',
      label: 'Boot-Loaders',
      items: ['boot-loaders/grub-legacy','boot-loaders/grub-2'],
    },
    {
      type: 'category',
      label: 'Команди за локализиране',
      items: ['location-commands/localization'],
    },
    {
      type: 'category',
      label: 'Файлови права',
      items: ['permissions/perm-basic','permissions/advanced-perm'],
    },
    {
      type: 'category',
      label: 'Процеси',
      items: ['processes/commands-processes'],
    },
    {
      type: 'category',
      label: 'Message Transfer Agent',
      items: ['emails/message-transfer-agent'],
    },
    {
      type: 'category',
      label: 'MS-Azure',
      items: ['ms-azure/introduction-azure'],
    },
    {
      type: 'category',
      label: 'Python',
      items: ['python/intro-python'],
    },
    {
      type: 'category',
      label: 'Test',
      items: ['test/test',{
        Test: ['test/test-new']
      }
    ],
    },
    {
      type: 'category',
      label: 'Bash-Scripting',
      items: ['intro-bash'],
    },
  ],
};
